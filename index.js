var config = require('./config.json'),
	monk = require('monk');

var db = monk(config.host[1]);
db.then(function (conn) {
	console.log();

	// findOneAndUpdate("aws_email_status", { "msgid": "0100015a45ed86ff-d23a6d62-5097-40e6-92b6-a1b65c47305e-000000" }, {
	// 	"response_status": "bounce",
	// 	"response_reject": {
	// 		"rejectType": "Permanent",
	// 		"rejectSubType": "General",
	// 		"action": "failed",
	// 		"status": "5.1.1",
	// 		"feedbackId": "0100015a45db9119-0a1afb54-c4ca-47a0-b6e2-327fc7cd7241-000000",
	// 		"timestamp": "2017-02-16T07:38:21.959Z"
	// 	}
	// }, function (err, data) {
	// 	console.log(err, data);
	// });
	
	findOneAndUpdate("testcollection", { "name": "Amit" }, { "age": "12", "sal": "101", "new": "ok" }, function(err, data) {
		console.log(err, data);
	});
}).catch(function (err) {
	console.log(err);
});

// require("./handlers/mongodbengine.js").connect(config.host[1], function(err, res) {
// 	console.log(err, res);
// });

function findOneAndUpdate(collection, find, update, callback) {
	db.get(collection).findOneAndUpdate(find, { $set: update }, callback);
}
