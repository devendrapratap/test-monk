var mongoose = require('mongoose');
var config = require("../config.json");
var debug = require('debug')('mongodb');

mongoose.Promise = Promise;
debug = console.log

function connect(url, callbackvalue) {
    var db = mongoose.createConnection(url, { server: { auto_reconnect: true } });

    // db.on('error', console.error.bind(console, 'connection error:'));
    db.on('error', function (err) {
        debug("Reconnecting from error");
        db = mongoose.createConnection(url, { server: { auto_reconnect: true } });

    });

    db.on('close', function (err, res) {
        debug("Reconnecting from close");
        process.exit(1);
        // db = mongoose.createConnection(url,{server:{auto_reconnect:true}});
    });

    db.once('open', function (ok) {
        function insert(schema, json, callbackvalue) {
            // var db = mongoose.connection;
            var objmodel = db.model(schema.options.collection, schema);

            debug("schema: " + schema.options.collection + " json: " + JSON.stringify(json));

            objmodel.create(json, function (err, result) {
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                if (args.length > 1)
                    callbackvalue(err, args);
                else
                    callbackvalue(err, args[0]);
            });
        }

        function update(schema, json, callbackvalue) {
            // var db = mongoose.connection;
            var objmodel = db.model(schema.options.collection, schema);

            debug("schema: " + schema.options.collection + " json: " + JSON.stringify(json));

            id = json._id;
            delete json._id;
            objmodel.update({ _id: id }, json, { upsert: false }, function (err, n, result) {

                callbackvalue(err, n, result);
            });
        }

        function updateQuery(schema, query, json, callbackvalue) {
            // var db = mongoose.connection;
            var objmodel = db.model(schema.options.collection, schema);

            debug("schema: " + schema.options.collection + " json: " + JSON.stringify(json) + " query: " + JSON.stringify(query));

            objmodel.update(query, json, { upsert: false }, function (err, n, result) {

                callbackvalue(err, n, result);
            });
        }

        function fetchOne(schema, query, callbackvalue) {
            // var db = mongoose.connection;
            var objmodel = db.model(schema.options.collection, schema);

            debug("schema: " + schema.options.collection + " query: " + JSON.stringify(query));

            objmodel.findOne(query, function (err, result) {

                callbackvalue(err, result);
            });
        }

        // function fetchAll(schema, query, callbackvalue)
        // {
        //     // var db = mongoose.connection;
        //     var objmodel = db.model(schema.options.collection, schema);

        //     debug("schema: " + schema.options.collection + " query: " + JSON.stringify(query));

        //     objmodel.find(query, function (err, result)
        //     {

        //         callbackvalue(err, result);
        //     });
        // }

        function fetchAll(schema, query, fields, options, callbackvalue) {
            // var db = mongoose.connection;
            var objmodel = db.model(schema.options.collection, schema);

            debug("schema: " + schema.options.collection + " query: " + JSON.stringify(query));

            objmodel.find(query, fields, options, function (err, result) {

                callbackvalue(err, result);
            });
        }

        function remove(schema, json, callbackvalue) {
            // var db = mongoose.connection;
            var objmodel = db.model(schema.options.collection, schema);

            debug("schema: " + schema.options.collection + " json: " + JSON.stringify(json));

            objmodel.remove({ _id: json._id }, function (err, result) {

                callbackvalue(err, result);
            });
        }

        function removeQuery(schema, query, callbackvalue) {
            // var db = mongoose.connection;
            var objmodel = db.model(schema.options.collection, schema);

            debug("schema: " + schema.options.collection + " json: " + JSON.stringify(query));

            objmodel.remove(query, function (err, result) {
                callbackvalue(err, result);
            });
        }

        function fetchOneAndUpdate(schema, query, update, options, callbackvalue) {
            // var db = mongoose.connection;
            var objmodel = db.model(schema.options.collection, schema);

            debug("schema: " + schema.options.collection + " query: " + JSON.stringify(query) + " Update: " + JSON.stringify(update) + " Options: " + JSON.stringify(options));

            objmodel.findOneAndUpdate(query, update, options, function (err, result) {

                callbackvalue(err, result);
            });
        }

        function mapReduce(schema, options, callbackvalue) {
            // var db = mongoose.connection;
            debug("schema: " + schema.options.collection + " options: ", options);
            var objmodel = db.model(schema.options.collection, schema);
            objmodel.mapReduce(options, callbackvalue);
        }

        function aggregate(schema, query, callbackvalue) {
            // var db = mongoose.connection;
            var objmodel = db.model(schema.options.collection, schema);

            debug("schema: " + schema.options.collection + " query: " + JSON.stringify(query));

            objmodel.aggregate(query, function (err, result) {

                callbackvalue(err, result);
            });
        }

        function getConnection() {
            return db;
        }

        // function getNextSequence(name, callbackvalue ) {

        // var db = mongoose.connection;
        //     var schema = counters.schema;
        //     var objmodel = db.model(schema.options.collection, schema);

        //     debug("schema: " + schema.options.collection + " Name: " + name);

        //     objmodel.findOneAndUpdate({ _id: name }, { $inc: { seq: 1 } }, { new: true}, function (err, result){
        //        callbackvalue(err, result);
        //     });
        // }
        callbackvalue(null, {
            connect: connect,
            insert: insert,
            remove: remove,
            update: update,
            fetchOne: fetchOne,
            fetchAll: fetchAll,
            fetchOneAndUpdate: fetchOneAndUpdate,
            mapReduce: mapReduce,
            mongoose: mongoose,
            removeQuery: removeQuery,
            aggregate: aggregate,
            updateQuery: updateQuery,
            getConnection: getConnection,
            // getNextSequence: getNextSequence
        });
    });
}

module.exports = {
    connect: connect
};